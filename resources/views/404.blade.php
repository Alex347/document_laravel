@extends('base')

@section('content')
    <br>
    <div class="col-sm-offset-4 col-sm-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">There is a problem !</h3>
            </div>
            <div class="panel-body">
                <p>We are sorry the page doesn't exist...</p>
            </div>
        </div>
    </div>
@endsection
