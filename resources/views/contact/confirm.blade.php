@extends('base')

@section('content')
    <br>
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-info">
            <div class="panel-heading">Contactez-moi</div>
            <div class="panel-body">
                Thanks, your message has been send to the admin. You will get an answer soon.
            </div>
        </div>
    </div>
@endsection
