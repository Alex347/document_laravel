@extends('base')

@section('header')
    @if(Auth::check())
        <div class="btn-group pull-right">
            {!! link_to_route('document.create', 'Create an article', [], ['class' => 'btn btn-info']) !!}
            {!! link_to('logout', 'Logout', ['class' => 'btn btn-warning']) !!}
        </div>
    @else
        {!! link_to('login', 'Connect', ['class' => 'btn btn-info pull-right']) !!}
    @endif
@endsection

@section('content')

    @if($post)

        <article class="row bg-primary-dark">
            <div class="col-md-12">

                <header>
                        <h3>{{$post->title}}</h3>
                </header>
                <hr>
                <section>
                    <p>{{ $post->description }}</p>
                    <em class="pull-right">
                        <span class="glyphicon glyphicon-pencil"></span> {{ $post->user->name }} le{!! $post->date !!}
                    </em>
                </section><br>

                    @if(Auth::check())
                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                Leave a comment
                            </a>

                        </p>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                {!! Form::open(['route' => 'commentary.store']) !!}
                                <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                                    {!! $errors->first('title', '<small class="help-block">:message</small>') !!}
                                </div>
                                <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                                    {!! Form::textarea ('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                                    {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                                </div>
                                {{ Form::hidden('document_id', $post->id ) }}
                                {!! Form::submit('Send', ['class' => 'btn btn-info pull-right']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div><br><br><br>
                    @endif

                <div class="float-right">
                    @if($comments)
                        @foreach($comments as $com)
                            <header><h5>Commentaire n° {{$com->id}}</h5></header>
                            <h6>{{$com->title}}</h6>
                            <p>{{$com->description}}</p>
                            <p>{{$com->date}}</p>
                            @endforeach
                        @endif
                </div><br>



            </div>
        </article>
        @endif
        <br>


@endsection
