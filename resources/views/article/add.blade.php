@extends('base')

@section('content')
    <br>
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-info">
            <div class="panel-heading">Add an article</div>
            <div class="panel-body">
                {!! Form::open(['route' => 'document.store', 'files' => true]) !!}
                <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                    {!! $errors->first('title', '<small class="help-block">:message</small>') !!}
                </div>
                <div class="form-group {!! $errors->has('category') ? 'has-error' : '' !!}">

                    <select name="category_id" id="category" class="form-control">
                        @foreach($categories as $id => $country)
                            <option value="{{ $id }}">
                                {{ $country -> name }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('category', '<small class="help-block">:message</small>') !!}
                </div>
                <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                    {!! Form::textarea ('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                    {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                </div>
                <div class="form-group {!! $errors->has('img') ? 'has-error' : '' !!}">
                    {!! Form::file('img', ['class' => 'form-control']) !!}
                    {!! $errors->first('img', '<small class="help-block">:message</small>') !!}
                </div>
                {!! Form::submit('Send', ['class' => 'btn btn-info pull-right']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
