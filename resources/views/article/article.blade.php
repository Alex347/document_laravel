@extends('base')

@section('content')
    @if(isset($info))
        <div class="row alert alert-info">{{ $info }}</div>
    @endif
    {!! $links !!}
    @foreach($posts as $post)
        <article class="row bg-primary-dark">
            <div class="col-md-12">
                <header>
{{--                    <h1>  {!! link_to('document/'.$post->id, $post->title) !!}--}}
                        <h1><a href="{{route('document.show', $post ?? '')}}"  role="button">{{$post->title}}</a> </h1>
                </header>
                <hr>
                <section>
                    <p>{{ $post->description }}</p>
                    @if(Auth::check() and Auth::user()->admin)
                        {!! Form::open(['method' => 'DELETE', 'route' => ['document.destroy', $post->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs ', 'onclick' => 'return confirm(\'Vraiment supprimer cet article ?\')']) !!}
                        {!! Form::close() !!}
                    @endif
                    <em class="pull-right">
                        <span class="glyphicon glyphicon-pencil"></span> {{ $post->user->name }} le{!! $post->date !!}
                    </em>
                </section>
            </div>
        </article>
        <br>
    @endforeach
{!! $links !!}
@endsection
