<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder {

    private function randDate()
    {
        return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
    }

    public function run()
    {
        DB::table('category')->delete();

        for($i = 0; $i < 3; ++$i)
        {
            $date = $this->randDate();
            DB::table('category')->insert([
                'name' => 'Catégory ' . $i,
            ]);
        }
    }
}

