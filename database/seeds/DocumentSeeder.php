<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DocumentSeeder extends Seeder {

    private function randDate()
    {
        return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
    }

    public function run()
    {
        DB::table('document')->delete();

        for($i = 0; $i < 50; ++$i)
        {
            $date = $this->randDate();
            DB::table('document')->insert([
                'title' => 'Title' . $i,
                'description' => 'Content: ' . $i . ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'user_id' => rand(1, 10),
                'category_id' => rand(1, 3),
                'img' => 'https://www.google.com/imgres?imgurl=http%3A%2F%2Foptic-box.com%2Fassets%2Fimg%2Fdemo%2F7.jpg&imgrefurl=http%3A%2F%2Foptic-box.com%2Fassets%2Fimg%2Fdemo%2F&tbnid=b6zl4NzpSSUegM&vet=12ahUKEwjlm7Xor5fpAhWPwoUKHVWPBBMQMygLegUIARCDAg..i&docid=bBaN1VjaHDmkIM&w=760&h=405&q=img&ved=2ahUKEwjlm7Xor5fpAhWPwoUKHVWPBBMQMygLegUIARCDAg',
                'date' => $date
            ]);
        }
    }
}
