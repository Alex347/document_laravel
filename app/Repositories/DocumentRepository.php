<?php

namespace App\Repositories;

use App\Document;
use Illuminate\Support\Facades\DB;

class DocumentRepository
{

    protected $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    public function getPaginate($n)
    {

        return $this->document->with('user')
            ->orderBy('document.date', 'desc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        DB::table('document')->insert([
            $inputs
        ]);
//        $this->document->create($inputs);
    }
    public function getById($id)
    {
        return $this->document->findOrFail($id);
    }
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

}
