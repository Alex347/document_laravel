<?php

namespace App\Repositories;

use App\Commentary;
use App\Document;
use Illuminate\Support\Facades\DB;

class CommentaryRepository
{

    protected $commentary;

    public function __construct(Commentary $commentary)
    {
        $this->commentary = $commentary;
    }

    public function getPaginate($n)
    {

        return $this->commentary->with('user')
            ->orderBy('commentary.date', 'desc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        DB::table('commentary')->insert([
            $inputs
        ]);
//        $this->document->create($inputs);
    }
    public function getById($id)
    {
        return $this->commentary->findOrFail($id);
    }
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

}
