<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentary extends Model
{
    protected $table = 'commentary';
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'date', 'document_id'];

    public function document()
    {
        return $this->belongsTo(Document::class);
    }
}
