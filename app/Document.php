<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'document';
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'date', 'img', 'category_id', 'user_id'];

    public function commentary()
    {
        return $this->hasMany(Commentary::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
