<?php

namespace App\Http\Controllers;

use App\Commentary;
use App\Repositories\CommentaryRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentaryController extends Controller
{

    protected $commentaryRepository;

    protected $nbrPerPage = 4;

    public function __construct(CommentaryRepository $commentaryRepository)
    {
        $this->middleware('auth', ['except' => 'index']);
        $this->middleware('admin', ['only' => 'destroy']);

        $this->commentaryRepository = $commentaryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Commentary[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $commentary = Commentary::all();
        return $commentary;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $inputs = array_merge($request->all(), ['user_id' => $request->user()->id]);
        if($inputs['_token']) {
            $inputs['date'] = Carbon::now();
            unset($inputs['_token']);
            $this->commentaryRepository->store($inputs);
        }
        return redirect()->back()->with('success', 'Commentary saved !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
