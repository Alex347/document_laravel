<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller {

    public function getForm()
    {
        return view('contact/contact');
    }

    public function postForm(ContactRequest $request)
    {

        Mail::send('contact/email_contact', $request->all(), function($message)
        {
            $message->to('ledain.alexis@gmail.com')->subject('Contact');
        });

        return view('contact/confirm');
    }

}
