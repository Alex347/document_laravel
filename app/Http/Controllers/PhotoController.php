<?php

namespace App\Http\Controllers;

use App\Gestion\PhotoGestion;
use App\Gestion\PhotoGestionInterface;
use App\Http\Requests\ImagesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PhotoController extends Controller
{

    public function getForm() {
        return view('article/photo');
    }

    public function postForm(ImagesRequest $request, PhotoGestionInterface $photogestion){

        if($photogestion->save($request->file('image'))) {
            return view('article/article');
        }
        return redirect('photo')
            ->with('error','Désolé mais votre image ne peut pas être envoyée !');

    }


}
