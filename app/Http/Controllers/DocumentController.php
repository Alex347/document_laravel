<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\Gestion\PhotoGestionInterface;
use App\Http\Requests\Commentary;
use App\Http\Requests\DocumentRequest;
use App\Http\Requests\ImagesRequest;
use App\Repositories\DocumentRepository;
use Carbon\Carbon;
use DemeterChain\C;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DocumentController extends Controller
{

    protected $documentRepository;

    protected $nbrPerPage = 4;

    public function __construct(DocumentRepository $documentRepository)
    {
        $this->middleware('auth', ['except' => 'index']);
        $this->middleware('admin', ['only' => 'destroy']);

        $this->documentRepository = $documentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $posts = $this->documentRepository->getPaginate($this->nbrPerPage);
        $links = $posts->render();

        return view('article.article', compact('posts', 'links'));
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function show($id)
    {
        $post = Document::find($id);
        $comments = $post->commentary;
//        dd($comments);

        return view('article.detail', compact('post', 'comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.add', compact('categories'));
    }

    /**
     * Creating a new resource.
     * @param Request $request
     * @param PhotoGestionInterface $photoGestion
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, PhotoGestionInterface $photoGestion)
    {
        $inputs = array_merge($request->all(), ['user_id' => $request->user()->id]);
        if($inputs['_token'] && $photoGestion->save($request->file('img'))) {
            $inputs['date'] = Carbon::now();
            $inputs['img'] = $request->file('img');
            unset($inputs['_token']);
            $this->documentRepository->store($inputs);
        }
            return redirect()->route('index')->with('success', 'Article successfully saved');

//        return redirect('photo')
//            ->with('error',"File can't be save !");
//        return redirect()->route('index')->with('success', 'Article successfully saved');
    }

    public function edit($id)
    {
        $contact = Document::find($id);
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
//        return $id;
        print($id);
        $this->documentRepository->destroy($id);

        return redirect()->route('index')->with('success', 'Corona Case is successfully saved');

    }

}
